﻿using LVL2_ASPNet_MVC_03.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_03.Controllers
{
    public class CustomerController : Controller
    {
        db_costumerEntities db = new db_costumerEntities();
        // GET: Customer
        public ActionResult Index()
        {
            return View(db.tbl_costumer.ToList());
        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {
            return View(db.tbl_costumer.Where(x=>x.Id == id).FirstOrDefault());
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(tbl_costumer costumer)
        {
            /*
            try
            {
                int a = 10;
                int b = 2;
                int c = a / b;
            }
            catch (DivideByZeroException msg)
            {
                ViewBag.Message = msg;
            }
            */
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    // TODO: Add insert logic here
                    // Insert new costumer [Process 1]
                    db.tbl_costumer.Add(costumer);
                    db.SaveChanges();
                    return RedirectToAction("Index");

                    /*
                    // [Process 2]
                    int a = 10;
                    int b = 2;
                    int c = a / b;

                    // update address with name Agung [Process 3]
                    tbl_costumer edit = db.tbl_costumer.Where(x => x.Id == 1).FirstOrDefault();
                    edit.address = "Lembang Bandung";
                    db.Entry(edit).State = EntityState.Modified;
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("Index");
                    */
                }
                catch (Exception msg)
                {
                    //    transaction.Rollback();

                    // "Comment dibawah ini merupakan metode memasukkan error kedalam database auditTrail"

                    //  var error_msg = msg;
                    //  string error_action = "create costumer";
                    //  string error_section = "create costumer";
                    //  string user_login = "budi123";
                    //  tbl_auditTrail err = new tbl_auditTrail
                    //  {
                    //      error_msg = error_msg.ToString(),
                    //      user_login = user_login,
                    //      error_date = Datetime.now,
                    //      error_action = error_action,
                    //      error_section = error_section
                    //  };
                    // db.Entry(err).State = EntityState.Added;
                    // db.SaveChanges();

                    ViewBag.Message = msg;
                    return View();
                }
            }
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int id)
        {
            return View(db.tbl_costumer.Where(x=>x.Id == id).FirstOrDefault());
        }

        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, tbl_costumer costumer)
        {
            try
            {
                // TODO: Add update logic here
                db.Entry(costumer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int id)
        {
            return View(db.tbl_costumer.Where(x => x.Id == id).FirstOrDefault());
        }

        // POST: Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, tbl_costumer costumer)
        {
            try
            {
                // TODO: Add delete logic here
                costumer = db.tbl_costumer.Where(x => x.Id == id).FirstOrDefault();
                db.tbl_costumer.Remove(costumer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
